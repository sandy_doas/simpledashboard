
import { PriorityList, IssueTypeList } from './Jira';
import { SummaryValue } from './SummaryValue';

export class Summary {
    public operation;
    public groupbycolumn;
    public columnname;
    public values: SummaryValue[];
}

