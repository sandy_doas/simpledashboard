export class MenuItem {
  id: number;
  name: string;
  routelink: string;
  routeParams: string
  submenu: MenuItem[];
}

export const MenuItems: MenuItem[] = [
    { id: 1, name: 'Dashboard', routelink: '/dashboard', routeParams: '', submenu: [] },
    { id: 2, name: 'Summary', routelink: '/summary', routeParams: '', submenu: [] },
    { id: 3, name: 'Combine Data', routelink: '/combine-data', routeParams: '', submenu: [] }
];
