
export  enum PriorityEnum {
    major = 0,
    minor = 1,
    meduim = 2
}

export  enum IssueTypeEnum{
    task = 0,
    improvement = 1,
    bug = 2
}

export const PriorityList = [
    'major',
    'minor',
    'meduim'
]

export const IssueTypeList = [
    'task',
    'improvement',
    'bug'
]

export class Jira {
   public id: number;
   public issuetype : IssueTypeEnum;
   public timestamp: Date;
   public components: string;
   public assignee: string;
   public priority: PriorityEnum;
   public open : number;
   public resolved: number;
   public inprogress:number;
   public closed : number;
}

export const JiraList : Jira[] =[

     {
    id: 35,
    timestamp: new Date(),
    issuetype: IssueTypeEnum.improvement,
    components: "Prototype",
    assignee: "asheirah",
    priority: PriorityEnum.major,
    resolved: 0,
    inprogress: 1,
    open: 0,
    closed: 0
  },
  {
    id: 36,
    timestamp: new Date(),
    issuetype: IssueTypeEnum.task,
    components: "Time-series data",
    assignee: "larabell",
    priority: PriorityEnum.major,
    resolved: 2,
    inprogress: 0,
    open: 0,
    closed: 0
  },
  {
    id: 37,
    timestamp: new Date(),
    issuetype: IssueTypeEnum.task,
    components: "Event management",
    assignee: "larabell",
    priority: PriorityEnum.major,
     resolved: 0,
    inprogress: 0,
    open: 1,
    closed: 0 
  },
  {
    id: 38,
    timestamp: new Date(),
    issuetype: IssueTypeEnum.task,
    components: "Regression Tracking",
    assignee: "larabell",
    priority: PriorityEnum.major,
    resolved: 0,
    inprogress: 0,
    open: 3,
    closed: 0
  },
  {
    id: 39,
    timestamp: new Date(),
    issuetype: IssueTypeEnum.task,
    components: "Visualization (generic widgets)",
    assignee: "larabell",
    priority: PriorityEnum.major,
    resolved: 0,
    inprogress: 0,
    open: 10,
    closed: 0
  }
]

