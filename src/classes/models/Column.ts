import { PriorityList, IssueTypeList } from './Jira';

export class Column {
    public name: string;
    public code: string;
    public isVisible: boolean;
    public options: string[];
    public filterValue: string;
}

export const Columns: Column[] = [
    { name: "Issue Type", code: "issuetype", isVisible: true, options: IssueTypeList, filterValue: "" },
    { name: "Assignee", code: "assignee", isVisible: true, options: [], filterValue: "" },
    { name: "Priority", code: "priority", isVisible: true, options: PriorityList, filterValue: "" },
    { name: "Time", code: "timestamp", isVisible: true, options: [], filterValue: "" },
    { name: "Components", code: "components", isVisible: true, options: [], filterValue: "" },
    { name: "Closed", code: "closed", isVisible: true, options: [], filterValue: "" },
    { name: "Opened", code: "open", isVisible: true, options: [], filterValue: "" },
    { name: "Resolved", code: "resolved", isVisible: true, options: [], filterValue: "" },
    { name: "In Progress", code: "inprogress", isVisible: true, options: [], filterValue: "" },
]
