<?php
require_once('DbConnection.php');

  $DB = new DbConnection();
  if($_GET['data'] == 'all'){
  $DB->getSampleDataFromDB();
  }else if($_GET['data'] == 'summary'){
  $DB->getJiraSummaryData();
  }else if($_GET['data'] == "createsummary"){
     $post= file_get_contents('php://input');
     $data = json_decode($post);
     $DB->createSummary($data->operation, $data->groupbycolumn, $data->columnname);
  }else if($_GET['data'] == "combine"){
     $post= file_get_contents('php://input');
     $data = json_decode($post);
     $DB->createCombinedData($data->combine, $data->jiracols, $data->svncols);
  }
?>