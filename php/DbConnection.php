<?php

class DbConnection {

    var $servername;
    var $username;
    var $password;
    var $dbname;

    function DbConnection() {
        $this->servername = "localhost";
        $this->username = "root";
        $this->password = "";
        $this->dbname = "dashboard";
    }

    function connect() {
        $conn = new mysqli($this->servername, $this->username, $this->password, $this->dbname);
        // Check connection
        if ($conn->connect_error) {
            die("Connection failed: " . $conn->connect_error);
        }
        return $conn;
    }

    function getSampleDataFromDB() {
        $data = array();
        $conn = $this->connect();
        $sql = "select * from jira";
        $result = $conn->query($sql);

        if ($result->num_rows > 0) {
            // output data of each row
            while ($row = $result->fetch_assoc()) {
                $object = new stdClass();
                $object->id = $row['id'];
                $object->issuetype = $row['issuetype'];
                $object->components = $row['components'];
                $object->assignee = $row['assignee'];
                $object->priority = $row['priority'];
                $object->resolved = $row['resolved'];
                $object->inprogress = $row['inprogress'];
                $object->open = $row['open'];
                $object->closed = $row['closed'];
                $object->timestamp = $row['timestamp'];
                $data[] = $object;
            }
        } else {
            echo "0 results";
        }
        echo json_encode($data);
        $conn->close();
    }

    function getAllJiraData() {
        $Data = array();
        $conn = $this->connect();
        $sql = "select * from jira";
        $result = mysqli_query($conn, $sql);
        if ($result->num_rows > 0) {
            // output data of each row
            while ($row = $result->fetch_assoc()) {
                $Data[] = $row;
            }
            mysqli_close($conn);
            print json_encode($Data);
        }
    }

    function getJiraSummaryData() {
        $summary = array();
        $conn = $this->connect();
        $sql = "select * from summary";
        $result = mysqli_query($conn, $sql);
        if ($result->num_rows > 0) {
            // output data of each row
            while ($row = $result->fetch_assoc()) {
                $query = "select ". $row['groupbycolumn'] . " as groupbycolumn , " . $row['operation'] . "(" . $row['columnname'] . ") as value from jira group by " . $row['groupbycolumn'];
                $summaryResult = mysqli_query($conn, $query);
                    $summaryObj = new stdClass();
                    $summaryObj->operation = $row['operation'];
                    $summaryObj->groupbycolumn = $row['groupbycolumn'];
                    $summaryObj->columnname = $row['columnname'];
                while ($myrow = $summaryResult->fetch_assoc()) {
                    $summaryObj->values[] = $myrow;
                }
                 $summary[] = $summaryObj;
            }
             
            mysqli_close($conn);
            print json_encode($summary);
        }
    }
    function createSummary($operation, $groupbycolumn, $columnname){
        $conn = $this->connect();
        $sql = "INSERT INTO summary (operation,groupbycolumn,columnname) VALUES ('". $operation . "','".$groupbycolumn ."', '" .$columnname. "')";
        $result = mysqli_query($conn, $sql);
        $this->getJiraSummaryData(); 
    }
   

  function createCombinedData($combine, $jiracols, $svncols){
        $data = array();
        $conditions = array();
          if($combine == 'assignee'){
           $condition = "jira.assignee = svn.owner";
          }else if($combine == 'timestamp'){
            $condition = "jira.timestamp = svn.timestamp";
          }
        $conn = $this->connect();
        $sql = "select distinct(jira.id) ,". implode(", jira. ", $jiracols) . " , ". implode(", svn.", $svncols) .", jira.". $combine . "  from jira inner join svn on ". $condition;
        $result = mysqli_query($conn, $sql);
        if ($result->num_rows > 0) {
            while ($row = $result->fetch_assoc()) {
               $data[] = $row;
            }
        }
            mysqli_close($conn);
            print json_encode($data);
  }

    function addSampleDataToDb() {
        $json = '[
  {
    "id": 35,
    "timestamp": "2016-04-15 16:37:40PDT",
    "issuetype": "Improvement",
    "components": "Prototype",
    "assignee": "asheirah",
    "priority": "Major",
    "resolved": 0,
    "inprogress": 1,
    "open": 0,
    "closed": 0
  },
  {
    "id": 36,
    "timestamp": "2016-04-15 16:37:40PDT",
    "issuetype": "Task",
    "components": "Time-series data",
    "assignee": "larabell",
    "priority": "Major",
    "resolved": 2,
    "inprogress": 0,
    "open": 0,
    "closed": 0
  },
  {
    "id": 37,
    "timestamp": "2016-04-15 16:37:40PDT",
    "issuetype": "Task",
    "components": "Event management",
    "assignee": "larabell",
    "priority": "Major",
    "resolved": 0,
    "inprogress": 0,
    "open": 1,
    "closed": 0
  },
  {
    "id": 38,
    "timestamp": "2016-04-15 16:37:40PDT",
    "issuetype": "Task",
    "components": "Regression Tracking",
    "assignee": "larabell",
    "priority": "Major",
    "resolved": 0,
    "inprogress": 0,
    "open": 3,
    "closed": 0
  },
  {
    "id": 39,
    "timestamp": "2016-04-15 16:37:40PDT",
    "issuetype": "Task",
    "components": "Visualization (generic widgets)",
    "assignee": "larabell",
    "priority": "Major",
    "resolved": 0,
    "inprogress": 0,
    "open": 10,
    "closed": 0
  },
  {
    "id": 40,
    "timestamp": "2016-04-15 16:37:40PDT",
    "issuetype": "Task",
    "components": "Database",
    "assignee": "larabell",
    "priority": "Major",
    "resolved": 0,
    "inprogress": 0,
    "open": 1,
    "closed": 0
  },
  {
    "id": 41,
    "timestamp": "2016-04-15 16:37:40PDT",
    "issuetype": "Task",
    "components": "Project/estimation analysis",
    "assignee": "larabell",
    "priority": "Major",
    "resolved": 0,
    "inprogress": 0,
    "open": 2,
    "closed": 0
  },
  {
    "id": 42,
    "timestamp": "2016-04-15 16:37:40PDT",
    "issuetype": "Task",
    "components": "CServer",
    "assignee": "larabell",
    "priority": "Major",
    "resolved": 4,
    "inprogress": 3,
    "open": 16,
    "closed": 0
  },
  {
    "id": 43,
    "timestamp": "2016-04-15 16:37:40PDT",
    "issuetype": "Task",
    "components": "Verification Portal (dashboard)",
    "assignee": "larabell",
    "priority": "Major",
    "resolved": 0,
    "inprogress": 0,
    "open": 2,
    "closed": 0
  },
  {
    "id": 44,
    "timestamp": "2016-04-15 16:37:40PDT",
    "issuetype": "Task",
    "components": "Testplan Management",
    "assignee": "None",
    "priority": "Major",
    "resolved": 0,
    "inprogress": 0,
    "open": 1,
    "closed": 0
  },
  {
    "id": 45,
    "timestamp": "2016-04-15 16:37:40PDT",
    "issuetype": "Task",
    "components": "Development Infrastructure",
    "assignee": "larabell",
    "priority": "Major",
    "resolved": 0,
    "inprogress": 0,
    "open": 2,
    "closed": 0
  },
  {
    "id": 46,
    "timestamp": "2016-04-15 16:37:40PDT",
    "issuetype": "Task",
    "components": "Licensing",
    "assignee": "larabell",
    "priority": "Major",
    "resolved": 0,
    "inprogress": 0,
    "open": 1,
    "closed": 0
  },
  {
    "id": 47,
    "timestamp": "2016-04-15 16:37:40PDT",
    "issuetype": "Task",
    "components": "Coverage/trending",
    "assignee": "larabell",
    "priority": "Major",
    "resolved": 0,
    "inprogress": 0,
    "open": 1,
    "closed": 0
  },
  {
    "id": 48,
    "timestamp": "2016-04-15 16:37:40PDT",
    "issuetype": "Task",
    "components": "Production testbed",
    "assignee": "larabell",
    "priority": "Major",
    "resolved": 0,
    "inprogress": 0,
    "open": 6,
    "closed": 0
  },
  {
    "id": 49,
    "timestamp": "2016-04-15 16:37:40PDT",
    "issuetype": "Task",
    "components": "Prototype",
    "assignee": "darronm",
    "priority": "Major",
    "resolved": 5,
    "inprogress": 2,
    "open": 3,
    "closed": 0
  },
  {
    "id": 50,
    "timestamp": "2016-04-15 16:37:40PDT",
    "issuetype": "Task",
    "components": "Prototype",
    "assignee": "None",
    "priority": "Major",
    "resolved": 0,
    "inprogress": 0,
    "open": 32,
    "closed": 0
  },
  {
    "id": 51,
    "timestamp": "2016-04-15 16:37:40PDT",
    "issuetype": "Task",
    "components": "Prototype",
    "assignee": "slaha",
    "priority": "Major",
    "resolved": 0,
    "inprogress": 1,
    "open": 0,
    "closed": 0
  },
  {
    "id": 52,
    "timestamp": "2016-04-15 16:37:40PDT",
    "issuetype": "Task",
    "components": "Prototype",
    "assignee": "larabell",
    "priority": "Major",
    "resolved": 6,
    "inprogress": 0,
    "open": 10,
    "closed": 0
  },
  {
    "id": 53,
    "timestamp": "2016-04-15 16:37:40PDT",
    "issuetype": "Task",
    "components": "Alerts and messaging",
    "assignee": "larabell",
    "priority": "Major",
    "resolved": 0,
    "inprogress": 0,
    "open": 1,
    "closed": 0
  },
  {
    "id": 34,
    "timestamp": "2016-04-15 16:37:40PDT",
    "issuetype": "Improvement",
    "components": "None",
    "assignee": "None",
    "priority": "Major",
    "resolved": 0,
    "inprogress": 0,
    "open": 0,
    "closed": 2
  }
]';

        $conn = $this->connect();
        $json = json_decode($json);
        foreach ($json as $key => $value) {
            $sql = "";
            $sql = "INSERT INTO jira (id, issuetype, timestamp , components, assignee, priority,resolved, inprogress, open, closed)
              VALUES (" . $value->id . " ,'" . $value->issuetype . "', '" . $value->timestamp . "', '" . $value->components . "', '" . $value->assignee . "', '" . $value->priority . "', '" . $value->resolved . "', '" . $value->inprogress . "', '" . $value->open . "', '" . $value->closed . "'  )";

            $result = $conn->query($sql);
        }
    }


    function addSampleSVNDataToDb() {
        $json = '
  [
  {
    "id": 2,
    "timestamp": "2016-06-08 06:41:12PDT",
    "file": "sv/cpu_wb_agent/cpu_wb_agent.svh",
    "type": "hdl",
    "owner": "darronm",
    "whitespace": 15,
    "comments": 70,
    "code": 22,
    "sloc": 107,
    "added": 0,
    "removed": 0,
    "modified": 0,
    "churn": 0
  },
  {
    "id": 3,
    "timestamp": "2016-06-08 06:41:12PDT",
    "file": "sv/cpu_wb_agent/cpu_wb_agent_pkg.sv",
    "type": "hdl",
    "owner": "darronm",
    "whitespace": 2,
    "comments": 1,
    "code": 7,
    "sloc": 10,
    "added": 0,
    "removed": 0,
    "modified": 0,
    "churn": 0
  },
  {
    "id": 4,
    "timestamp": "2016-06-08 06:41:12PDT",
    "file": "sv/cpu_wb_agent/wb_transaction.svh",
    "type": "hdl",
    "owner": "darronm",
    "whitespace": 11,
    "comments": 19,
    "code": 55,
    "sloc": 85,
    "added": 0,
    "removed": 0,
    "modified": 0,
    "churn": 0
  },
  {
    "id": 5,
    "timestamp": "2016-06-08 06:41:12PDT",
    "file": "sv/eth_mii_agent/crc_gen.svh",
    "type": "hdl",
    "owner": "darronm",
    "whitespace": 4,
    "comments": 159,
    "code": 9,
    "sloc": 172,
    "added": 0,
    "removed": 0,
    "modified": 0,
    "churn": 0
  },
  {
    "id": 6,
    "timestamp": "2016-06-08 06:41:12PDT",
    "file": "sv/eth_mii_agent/eth_mii_agent_pkg.sv",
    "type": "hdl",
    "owner": "darronm",
    "whitespace": 10,
    "comments": 23,
    "code": 37,
    "sloc": 70,
    "added": 0,
    "removed": 0,
    "modified": 0,
    "churn": 0
  },
  {
    "id": 7,
    "timestamp": "2016-06-08 06:41:12PDT",
    "file": "sv/eth_mii_agent/eth_mii_if.sv",
    "type": "hdl",
    "owner": "darronm",
    "whitespace": 12,
    "comments": 30,
    "code": 46,
    "sloc": 88,
    "added": 0,
    "removed": 0,
    "modified": 0,
    "churn": 0
  },
  {
    "id": 8,
    "timestamp": "2016-06-08 06:41:12PDT",
    "file": "sv/eth_mii_agent/eth_mii_rx_agent.svh",
    "type": "hdl",
    "owner": "darronm",
    "whitespace": 16,
    "comments": 29,
    "code": 28,
    "sloc": 73,
    "added": 0,
    "removed": 0,
    "modified": 0,
    "churn": 0
  },
  {
    "id": 9,
    "timestamp": "2016-06-08 06:41:12PDT",
    "file": "sv/eth_mii_agent/eth_mii_rx_cover.svh",
    "type": "hdl",
    "owner": "darronm",
    "whitespace": 17,
    "comments": 17,
    "code": 72,
    "sloc": 106,
    "added": 0,
    "removed": 0,
    "modified": 0,
    "churn": 0
  },
  {
    "id": 10,
    "timestamp": "2016-06-08 06:41:12PDT",
    "file": "sv/eth_mii_agent/eth_mii_rx_driver.svh",
    "type": "hdl",
    "owner": "darronm",
    "whitespace": 26,
    "comments": 40,
    "code": 65,
    "sloc": 131,
    "added": 0,
    "removed": 0,
    "modified": 0,
    "churn": 0
  },
  {
    "id": 11,
    "timestamp": "2016-06-08 06:41:12PDT",
    "file": "sv/eth_mii_agent/eth_mii_rx_monitor.svh",
    "type": "hdl",
    "owner": "darronm",
    "whitespace": 27,
    "comments": 42,
    "code": 126,
    "sloc": 195,
    "added": 0,
    "removed": 0,
    "modified": 0,
    "churn": 0
  },
  {
    "id": 12,
    "timestamp": "2016-06-08 06:41:12PDT",
    "file": "sv/eth_mii_agent/eth_mii_rx_sequence.svh",
    "type": "hdl",
    "owner": "darronm",
    "whitespace": 12,
    "comments": 82,
    "code": 17,
    "sloc": 111,
    "added": 0,
    "removed": 0,
    "modified": 0,
    "churn": 0
  },
  {
    "id": 13,
    "timestamp": "2016-06-08 06:41:12PDT",
    "file": "sv/eth_mii_agent/eth_mii_tx_agent.svh",
    "type": "hdl",
    "owner": "darronm",
    "whitespace": 15,
    "comments": 27,
    "code": 23,
    "sloc": 65,
    "added": 0,
    "removed": 0,
    "modified": 0,
    "churn": 0
  },
  {
    "id": 14,
    "timestamp": "2016-06-08 06:41:12PDT",
    "file": "sv/eth_mii_agent/eth_mii_tx_cover.svh",
    "type": "hdl",
    "owner": "darronm",
    "whitespace": 18,
    "comments": 18,
    "code": 74,
    "sloc": 110,
    "added": 0,
    "removed": 0,
    "modified": 0,
    "churn": 0
  },
  {
    "id": 15,
    "timestamp": "2016-06-08 06:41:12PDT",
    "file": "sv/eth_mii_agent/eth_mii_tx_ctrl_driver.svh",
    "type": "hdl",
    "owner": "darronm",
    "whitespace": 31,
    "comments": 26,
    "code": 74,
    "sloc": 131,
    "added": 0,
    "removed": 0,
    "modified": 0,
    "churn": 0
  },
  {
    "id": 16,
    "timestamp": "2016-06-08 06:41:12PDT",
    "file": "sv/eth_mii_agent/eth_mii_tx_ctrl_seq_item.svh",
    "type": "hdl",
    "owner": "darronm",
    "whitespace": 7,
    "comments": 16,
    "code": 9,
    "sloc": 32,
    "added": 0,
    "removed": 0,
    "modified": 0,
    "churn": 0
  },
  {
    "id": 17,
    "timestamp": "2016-06-08 06:41:12PDT",
    "file": "sv/eth_mii_agent/eth_mii_tx_driver.svh",
    "type": "hdl",
    "owner": "darronm",
    "whitespace": 27,
    "comments": 23,
    "code": 103,
    "sloc": 153,
    "added": 0,
    "removed": 0,
    "modified": 0,
    "churn": 0
  },
  {
    "id": 18,
    "timestamp": "2016-06-08 06:41:12PDT",
    "file": "sv/eth_mii_agent/eth_mii_tx_monitor.svh",
    "type": "hdl",
    "owner": "darronm",
    "whitespace": 26,
    "comments": 27,
    "code": 93,
    "sloc": 146,
    "added": 0,
    "removed": 0,
    "modified": 0,
    "churn": 0
  },
  {
    "id": 19,
    "timestamp": "2016-06-08 06:41:12PDT",
    "file": "sv/eth_mii_agent/eth_mii_tx_sequence.svh",
    "type": "hdl",
    "owner": "darronm",
    "whitespace": 11,
    "comments": 17,
    "code": 17,
    "sloc": 45,
    "added": 0,
    "removed": 0,
    "modified": 0,
    "churn": 0
  },
  {
    "id": 20,
    "timestamp": "2016-06-08 06:41:12PDT",
    "file": "sv/eth_mii_agent/eth_packet.svh",
    "type": "hdl",
    "owner": "darronm",
    "whitespace": 20,
    "comments": 42,
    "code": 72,
    "sloc": 134,
    "added": 0,
    "removed": 0,
    "modified": 0,
    "churn": 0
  },
  {
    "id": 21,
    "timestamp": "2016-06-08 06:41:12PDT",
    "file": "sv/eth_mii_agent/eth_rx_frame.svh",
    "type": "hdl",
    "owner": "darronm",
    "whitespace": 6,
    "comments": 18,
    "code": 19,
    "sloc": 43,
    "added": 0,
    "removed": 0,
    "modified": 0,
    "churn": 0
  }
]';

        $conn = $this->connect();
        $json = json_decode($json);
        foreach ($json as $key => $value) {
            $sql = "";
            $sql = "INSERT INTO svn (added, churn, code, comments,file, id, modified, owner, removed, sloc, timestamp, type, whitespace)
              VALUES (" . $value->added . " ,'" . $value->churn . "', '" . $value->code . "', '" . $value->comments . "', '" . $value->file . "', '" . $value->id . "', '" . $value->modified . "', '" . $value->owner . "', '" . $value->removed . "', '" . $value->sloc . "', '" . $value->timestamp . "', '" . $value->type . "', '" . $value->whitespace . "')";

            $result = $conn->query($sql);
        }
    }

}

?>