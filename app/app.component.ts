import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { HeaderComponent } from './header.component';
import { FooterComponent } from './footer.component';
/// <reference path="../typings/index.d.ts" />
/// <reference path="jquery.d.ts" />

@Component({
  selector: 'app',
  templateUrl: 'app/app.component.html',
  directives: [HeaderComponent, FooterComponent]
})

export class AppComponent implements OnInit {

  ngOnInit(): void {
 
  }

}
