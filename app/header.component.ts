/// <reference path="jquery.d.ts" />
import { Component, OnInit, AfterViewInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { MenuItem } from '../src/classes/models/menuItem';
import { MenuItems } from '../src/classes/models/menuItem';

@Component({
  selector: 'headerComponent',
  templateUrl: 'app/header.component.html',
})

export class HeaderComponent implements OnInit, AfterViewInit {
  public menuItems: MenuItem[];
  public currUrl: string;
  constructor(private router: Router,
    private route: ActivatedRoute) {
    this.menuItems = MenuItems;
  }

  ngAfterViewInit(): void {
  }

  isActiveTab(tabUrl) {
    if (tabUrl != '' && (this.currUrl == tabUrl)) {
      return true;
    } else {
      return false;
    }
  }
  goto(routelink: string): void {
    if (routelink != '') {
      let link = [routelink];
      this.router.navigate(link);
    }
  }
  ngOnInit(): void {
    this.router.events.subscribe((val) => {
      if (val.url != '') {
        this.currUrl = val.url;
      } 
      else if (val.urlAfterRedirects != '') {
        this.currUrl = val.urlAfterRedirects;
      } else if (val.url == '') {
        this.router.navigate(['/dashboard']);
      }
    });
  }

}
