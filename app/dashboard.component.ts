import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { JiraList, Jira, PriorityEnum, IssueTypeEnum } from '../src/classes/models/Jira';
import { Columns, Column } from '../src/classes/models/Column';
import { Request } from './request.service';
declare var $: JQueryStatic;

@Component({
    selector: 'dashboard',
    templateUrl: 'app/dashboard.component.html',
    providers: [Request]
})

export class DashboardComponent implements OnInit {
    public isDisabled: boolean;
    public Data: Jira[];
    public backupData: Jira[];
    public columns: Column[];
    public issueTypeList = IssueTypeEnum;
    public priorityList = PriorityEnum;

    constructor(private request: Request) {
        this.isDisabled = false;
        // this.Data = JiraList;
        this.columns = Columns;
        this.Data = this.getData();
        this.backupData = this.Data;
    }
    ngOnInit(): void {

    }
    getData() {
        let item = new Jira();
        let jiraList = [];
        this.request.setRequestParameters('php/api.php?data=all', '');
        this.request.getList().subscribe((data) => {
            data.forEach(element => {
                console.log(element);
                item = new Jira();
                item.id = element.id;
                item.issuetype = this.issueTypeList[element.issuetype.toLowerCase()];
                item.timestamp = new Date(element.timestamp);
                item.timestamp = item.timestamp.getDate() +'-'+ (item.timestamp.getMonth()+1) +'-'+ item.timestamp.getFullYear()+ ' ' + item.timestamp.getHours() + ':'+ item.timestamp.getMinutes() + ':' + item.timestamp.getSeconds();
                item.components = element.components;
                item.assignee = element.assignee;
                item.priority = this.priorityList[element.priority.toLowerCase()];
                item.open = element.open;
                item.resolved = element.resolved;
                item.inprogress = element.inprogress;
                item.closed = element.closed;
                jiraList.push(item);
            });
        });
        return jiraList;
    }
    resetFilters() {
        this.Data = this.backupData;
        let i = 0;
          this.columns.forEach(col => {
            $("#filter" + i).val('');
            i++;
        });
    }

    showHiddenRows() {
        this.columns.forEach(element => {
            element.isVisible = true;
        });
    }
    showAndHideColumn(index, isVisible) {
        this.columns[index].isVisible = !isVisible;
    }

    checkIfVisible(colCode) {
        this.columns.forEach(col => {
            if (col.code == colCode) {
                return col.isVisible;
            }
        });
    }

    filterAll() {
        this.Data = this.backupData;
        let tempData = [];
        let temColumns = [];
        let filter = [];
        let i = 0;
        this.columns.forEach(col => {
            var value = $("#filter" + i).val();
            if (value != '' && value != null && value != undefined) {
                col.filterValue = value;
                temColumns.push(col);

            }
            i++;
        });
        this.Data.forEach(element => {
            let add = true;
            temColumns.forEach(tempCol => {
                if (tempCol.filterValue != '') {
                    let elementvalue = String(element[tempCol.code]);
                    let filterValue = String(tempCol.filterValue);
                    if (elementvalue != filterValue && !elementvalue.toLowerCase().includes(filterValue.toLowerCase())) {
                        add = false;
                    }
                }
            });
            if (add == true) {
                tempData.push(element);
            }
        });
        this.Data = tempData;
    }

    sort(colIndex, ascOrder) {
        let colCode = this.columns[colIndex].code;
        let alph = true;
        if (colCode == "open" || colCode == "resolved" || colCode == "inprogress" || colCode == "closed" || colCode =="issuetype" || colCode =="priority") {
            alph = false;
        }

        if (alph == false) {
            this.Data.sort(function (a, b) {
                if (a[colCode] > b[colCode]) {
                    if (ascOrder == false) {
                        return -1;
                    } else {
                        return 1;
                    }
                }
                if (a[colCode] < b[colCode]) {
                    if (ascOrder == false) {
                        return 1;
                    } else {
                        return -1;
                    }
                }
                // a must be equal to b
                return 0;
            });
        } else {
            this.Data.sort(function (a, b) {
                let A = a[colCode].toUpperCase(); // ignore upper and lowercase
                let B = b[colCode].toUpperCase(); // ignore upper and lowercase

                if (A < B) {
                    if (ascOrder == false) {
                        return 1;
                    } else {
                        return -1;
                    }
                }
                if (A > B) {
                    if (ascOrder == false) {
                        return -1;
                    } else {
                        return 1;
                    }
                }
                // names must be equal
                return 0;
            });
        }

    }


}
