import { Injectable } from '@angular/core';
import { Headers, Http } from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';

@Injectable()
export class Request {
  private url = '';  // URL to web api
  private headers = new Headers({ 'Content-Type': 'application/json' });
  private body;

  constructor(private http: Http) { }

  setRequestParameters(url: string, body) { //, headers: JSON, body: JSON){
    this.url = url;
    this.headers = new Headers({ 'Content-Type': 'application/json' });
    this.body = body;
  }

  get() {
    return this.http.get(this.url, {
      headers: this.headers
    }).map(response => response.json())
  }

  getList() {
    return this.http.get(this.url, this.headers)
      .map(response => response.json());
  }


  delete() {
    return this.http.delete(this.url, { headers: this.headers })
      .toPromise()
      .then(() => null)
      .catch(this.handleError);
  }

  create() {
    return this.http
      .post(this.url, JSON.stringify(this.body), { headers: this.headers })
      .map(response => response.json());
  }
  getFilteredList() {
    return this.http
      .post(this.url, JSON.stringify(this.body), { headers: this.headers })
      .map(response => response.json());
  }
  update() {
    let res = '';
    return this.http
      .put(this.url, JSON.stringify(this.body), { headers: this.headers })
      .toPromise()
      .then(() => res)
      .catch(this.handleError);
  }

  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error); // for demo purposes only
    return Promise.reject(error.message || error);
  }
}
