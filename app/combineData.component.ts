import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { JiraList, Jira, PriorityEnum, IssueTypeEnum } from '../src/classes/models/Jira';
import { Columns, Column } from '../src/classes/models/Column';
import { Summary } from '../src/classes/models/Summary';
import { Request } from './request.service';
declare var $: JQueryStatic;

@Component({
    selector: 'combine-data',
    templateUrl: 'app/combineData.component.html',
    providers: [Request]
})

export class CombineDataComponent implements OnInit {
    public isDisabled: boolean;
    public rows;
    public columns: string[];
    public jiraSelected: string[];
    public combinebySelected: string;
    public svnSelected: string[];

    constructor(private request: Request) {
        this.isDisabled = false;
        this.combinebySelected = "assignee";
        this.rows = [];
        this.columns = [];
        this.jiraSelected = [];
        this.svnSelected = [];
    }
    setCombine(value) {
        this.combinebySelected = value;
    }
    setSelected(colname, options) {
        let selected = Array.apply(null, options)  // convert to real array
            .filter(option => option.selected)
            .map(option => option.value);
        if (colname == 'jiracols') {
            this.jiraSelected = selected;
        } else if (colname == 'svncols') {
            this.svnSelected = selected;
        }
    }

    combineData() {
        if (this.jiraSelected.length == 0) {
            alert('Please, Select Jira Columns');
        }else if (this.svnSelected.length == 0) {
            alert('Please, Select SVN Columns');
        }
        let item = new Summary();
        this.rows = [];
        let body = { combine: this.combinebySelected, jiracols: this.jiraSelected, svncols: this.svnSelected };
        this.request.setRequestParameters('php/api.php?data=combine', body);
        this.request.getFilteredList().subscribe((data) => {
            data.forEach(element => {
                this.rows.push(element);
            });
        });
        this.columns = [];
        this.columns.push(this.combinebySelected);
        this.columns = this.columns.concat(this.jiraSelected, this.svnSelected);
    }

    ngOnInit(): void {

    }

}
