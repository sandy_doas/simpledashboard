import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent } from './dashboard.component';
import { SummaryComponent } from './summary.component';
import { CombineDataComponent } from './combineData.component';
const appRoutes: Routes = [
  {
    path: 'dashboard',
    component: DashboardComponent,
  },
  {
    path: 'summary',
    component: SummaryComponent,
  },
  {
    path: 'combine-data',
    component: CombineDataComponent,
  },
  {
    path: '',
    redirectTo: '/dashboard',
    pathMatch: 'full'
  },

];

export const routing: ModuleWithProviders = RouterModule.forRoot(appRoutes);