import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { JiraList, Jira, PriorityEnum, IssueTypeEnum } from '../src/classes/models/Jira';
import { Columns, Column } from '../src/classes/models/Column';
import { Summary } from '../src/classes/models/Summary';
import { Request } from './request.service';
declare var $: JQueryStatic;

@Component({
    selector: 'summary',
    templateUrl: 'app/summary.component.html',
    providers: [Request]
})

export class SummaryComponent implements OnInit {
    public isDisabled: boolean;
    public summaryList: Summary[];

    constructor(private request: Request) {
        this.isDisabled = false;
        this.summaryList = this.getSummaryData();
    }
    ngOnInit(): void {

    }
    getSummaryData() {
        let item = new Summary();
        let summary = [];
        this.request.setRequestParameters('php/api.php?data=summary', '');
        this.request.getList().subscribe((data) => {
            console.log(data);
            data.forEach(element => {
                item = new Summary();
                item.operation = element.operation;
                item.columnname = element.columnname;
                item.groupbycolumn = element.groupbycolumn;
                item.values = element.values;
                summary.push(item);
            });
        });
        return summary;
    }

    createSummary(operation, columnname, groupbycolumn) {
        let item = new Summary();
        let summary = [];
        let body = {operation: operation , columnname: columnname , groupbycolumn: groupbycolumn};
        this.request.setRequestParameters('php/api.php?data=createsummary', body);
        this.request.create().subscribe((data) => {
            data.forEach(element => {
                item = new Summary();
                item.operation = element.operation;
                item.columnname = element.columnname;
                item.groupbycolumn = element.groupbycolumn;
                item.values = element.values;
                summary.push(item);
            });
        });
        this.summaryList = summary;
    }

}
